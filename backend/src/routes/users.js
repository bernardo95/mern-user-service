const { Router } = require('express');
const router = Router();

const { verificationToken } = require('../middlewares/autenticationJWT');
// Deconstruct
const userCtrl = require('../controllers/users.controller');

router.get('/getUsers', [], userCtrl.getUsers);
router.get('/getEmployees', [], userCtrl.getEmployees);
router.post('/createUser', [], userCtrl.createUser);
router.get('/getUser/:id', [], userCtrl.getUser);
router.post('/loginUser', [], userCtrl.loginUser);
router.delete('/deleteUser/:id', [], userCtrl.deleteUser);

module.exports = router;