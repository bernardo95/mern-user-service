const { Router } = require('express');
const router = Router();

const serviceCtrl = require('../controllers/service.controller');

router.get('/getServices', [], serviceCtrl.getServices);
router.get('/getServicesByEmployee/:idEmployee', [], serviceCtrl.getServicesByEmployee);
router.get('/getServicesByClient/:idClient', [], serviceCtrl.getServicesByClient);
router.post('/createService', [], serviceCtrl.createService);
router.get('/getService/:id', [], serviceCtrl.getService);
router.put('/updateService', [], serviceCtrl.updateService);
router.delete('/deleteService/:id', [], serviceCtrl.deleteService);

module.exports = router;